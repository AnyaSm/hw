
<form name="search" action="<?php echo home_url( '/' ) ?>" method="get" class="search-form col-sm-4 col-xs-12 end-sm start-xs">
    <div class="form-wrap">
        <input type="text" placeholder="Search" autocomplete="off" value="<?php echo get_search_query() ?>"
               name="s" id="text" class="input">
        <button type="submit" class="for-search"></button>
    </div>
</form>