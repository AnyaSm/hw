<?php

get_header(); ?>
    <section class="content">
        <div class="container">
            <?php
            // set up or arguments for our custom query (for pagination)
            $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
            $query_args = array(
                'post_type' => 'post',
                'posts_per_page' => 4,
                'paged' => $paged
            );
            // create a new instance of WP_Query
            $the_query = new WP_Query( $query_args );
            ?>
            <?php if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>

                    <article class="post col-sm-8">
                        <div class="img-wrap">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                            </a>
                        </div>
                        <h2>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h2>
                        <?php the_content(); ?>
                        <a class="button hvr-grow" href="<?php the_permalink(); ?>">Continue Reading</a>
                    </article>
                <?php endwhile; ?>

            <?php else: ?>
                <p>No posts found</p>
            <?php endif; ?>

            <?php get_sidebar(); ?>

        </div>

        <!-- post pagination-->

    </section>

<?php get_footer(); ?>