<?php
/*
Template Name: Home page
*/
get_header(); ?>
    <div class="main-content">
        <div class="container">
            <section class="content row col-sm-8 col-xs-12">

                <div class="slider col-xs-12">
                    <?php echo do_shortcode("[image-carousel interval=12000]"); ?>
                </div>

                <div class="title col-xs-12">
                    <h2>Latest blog post</h2>
                </div>
                <?php
                // set up or arguments for our custom query (for pagination)
                $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
                $query_args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 4,
                    'paged' => $paged
                );
                // create a new instance of WP_Query
                $the_query = new WP_Query( $query_args );
                ?>
                <?php query_posts($query_string . '&cat=-9'); ?>
                <?php if ( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>

                        <article class="post">
                            <div class="date-wrap col-xs-2">
                                <div class="date">
                                    <span><?php the_time( 'j' ); ?></span>
                                    <span class="month"><?php the_time( 'F' ); ?></span>
                                </div>
                            </div>
                            <div class="post-wrap col-xs-10">
                                <h2>
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                                <div class="post-comments">
                                    <a href="<?php the_permalink() ?>#comments">
                                        <?php comments_number('0 comments', '1 comment', '% comments'); ?>
                                    </a>
                                </div>
                                <?php the_content(); ?>
                                <a class="button" href="<?php the_permalink(); ?>">Continue Reading</a>
                            </div>
                        </article>
                    <?php endwhile; ?>

                    <div class="custom-pag col-xs-12 end-xs">
                        <?php
                        if (function_exists(custom_pagination)) {
                            custom_pagination($custom_query->max_num_pages,"",$paged);
                        }
                        ?>
                        <?php wp_reset_postdata(); ?>
                    </div>

                <?php else: ?>
                    <p>No posts found</p>
                <?php endif; ?>



            </section>
            <?php get_sidebar(); ?>


        </div>
    </div>

<?php get_footer(); ?>