<?php

get_header(); ?>
    <div class="main-content">
        <div class="container">
            <section class="content row col-sm-8 col-xs-12">
                <div class="title col-xs-12">
                    <h2><?php single_cat_title(); ?></h2>
                </div>
                <?php if (have_posts()):
                    while (have_posts()): the_post(); ?>
                        <?php setPostViews(get_the_ID()); ?>
                        <article class="post">
                            <div class="date-wrap col-xs-2">
                                <div class="date">
                                    <span><?php the_time( 'j' ); ?></span>
                                    <span class="month"><?php the_time( 'F' ); ?></span>
                                </div>
                            </div>
                            <div class="post-wrap col-xs-10">
                                <h2>
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h2>
                                <div class="post-comments">
                                    <a href="<?php the_permalink() ?>#comments">
                                        <?php comments_number('0 comments', '1 comment', '% comments'); ?>
                                    </a>
                                </div>
                                <?php the_content(); ?>
                                <a class="button" href="<?php the_permalink(); ?>">Continue Reading</a>
                            </div>
                        </article>
                    <?php endwhile; ?>

                <?php else: ?>
                    <p>No posts found</p>
                <?php endif; ?>

                <!-- post pagination-->
                <div class="pag-wrap col-xs-12 end-xs">
                    <?php
                    global $wp_query;

                    $big = 999999999; // need an unlikely integer

                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'total' => $wp_query->max_num_pages,
                        'prev_text' => '',
                        'next_text' => ''
                    ) );
                    ?>
                </div>

            </section>
            <?php get_sidebar(); ?>


        </div>
    </div>

<?php get_footer(); ?>