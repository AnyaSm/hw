<?php
/*
Template Name: Contact page
*/
get_header(); ?>
    <section class="contact">
        <div class="container">
            <div class="contact-wrap col-sm-8 col-xs-12">
                <div class="about-us row col-xs-12 middle-xs">
                    <div class="for-cont col-sm-4 col-xs-6">
                        <?php
                        query_posts('p=111');
                        while ( have_posts() ) : the_post(); ?>
                            <h2>
                                <?php the_title(); ?>
                            </h2>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    </div>
                    <div class="img-wrap col-sm-8 col-xs-6">
                        <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                    </div>
                </div>
                <div class="form col-xs-12">
                    <?php echo do_shortcode ('[contact-form-7 id="110" title="Contact"]'); ?>
                </div>
            </div>
            <aside class="info-sidebar col-sm-4 col-xs-12">
                <ul>
                    <?php if(!dynamic_sidebar('contact-sidebar')) : ?>

                    <?php endif; ?>
                </ul>
            </aside>

        </div>
    </section>

<?php get_footer(); ?>
