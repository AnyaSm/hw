<?php

get_header(); ?>
    <div class="main-content">
        <div class="container">
            <section class="content row col-sm-8 col-xs-12">
                <div class="title col-xs-12">
                    <h2><?php single_cat_title(); ?> - Gallery</h2>
                </div>
                <?php query_posts('posts_per_page=8&cat=-9'); ?>
                <?php if (have_posts()):
                    while (have_posts()): the_post(); ?>
                        <?php setPostViews(get_the_ID()); ?>
                        <article class="post post-category col-sm-6 col-xs-12">
                            <div class="post-wrap col-xs-12">
                                <div class="img-wrap">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                                    </a>
                                    <h2>
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h2>
                                    <div class="cont">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php endwhile; ?>

                <?php else: ?>
                    <p>No posts found</p>
                <?php endif; ?>

                <!-- post pagination-->
                <div class="pag-wrap col-sm-12 end-xs">
                    <?php
                    global $wp_query;

                    $big = 999999999; // need an unlikely integer

                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'total' => $wp_query->max_num_pages,
                        'prev_text' => '',
                        'next_text' => ''
                    ) );
                    ?>
                </div>

                <!-- post pagination-->

            </section>
            <?php get_sidebar(); ?>


        </div>
    </div>

<?php get_footer(); ?>