    <footer class="footer">
        <div class="container">
            <ul class="col-sm-6 start-xs">
                <?php if(!dynamic_sidebar('footer-sidebar')) : ?>

                <?php endif; ?>
            </ul>


            <ul  class="social row col-sm-6 end-xs" >
                <li>
                    <a href="<?php echo get_theme_mod('social_links_facebook'); ?>" class="facebook"></a>
                </li>
                <li>
                    <a href="<?php echo get_theme_mod('social_links_twitter'); ?>" class="twitter"></a>
                </li>
                <li>
                    <a href="<?php echo get_theme_mod('social_links_instagram'); ?>" class="instagram"></a>
                </li>
            </ul>
        </div>
    </footer>

    <?php wp_footer(); ?>

</body>
</html>