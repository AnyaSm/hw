<aside class="sidebar col-sm-4 col-xs-12">
    <ul>
        <li class="contact-form">
            <h3>Sign Up for Newsletter</h3>
            <?php echo do_shortcode ('[contact-form-7 id="85" title="Contact form 1"]'); ?>
        </li>
        <li>
            <div class="popular">
                <h3 class="widget-title">Most Popular</h3>
                <ul>
                    <?php
                    $args = array( numberposts => 5, meta_key => post_views_count, orderby => meta_value_num, order => DESC );
                    query_posts($args);
                    while ( have_posts() ) : the_post();
                    ?>
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endwhile; wp_reset_query(); ?>
                </ul>
            </div>
        </li>
        <?php if(!dynamic_sidebar('sidebar')) : ?>
            <li>

            </li>
        <?php endif; ?>
    </ul>
</aside>
